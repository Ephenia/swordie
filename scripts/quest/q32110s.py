# [Ellinel Fairy Academy] Combing the Academy 1 (32110)

COOTIE_THE_REALLY_SMALL = 1500011 # NPC ID

sm.setSpeakerID(COOTIE_THE_REALLY_SMALL)
sm.sendNext("Isn't this place amazing, #h # "
            "Let's have a look around.")

sm.setPlayerAsSpeaker()
sm.sendSay("What should we do first?")

sm.setSpeakerID(COOTIE_THE_REALLY_SMALL)
sm.sendSay("You know what kids love the most? "
            "Secrets! "
            "I remember trading potion recipes with my friends behind the teacher's backs, hiding away my alchemy research in the nooks around school...")

answer = sm.sendNext("I bet these kids have hidden notes all around the school. "
                     "But how would we find them?\r\n"
                     "#L1##bFind the children and ask them where they hid things. #k#l\r\n"
                     "#L2##bThey must be nearby, we should look around. #k#l\r\n"
                     "#L3##bI have no idea. This is hard... #k#l")

if answer == 1:
    sm.sendNext("What a human thing to say, #n #. "
                "We have to find their secrets FIRST! "
                "Then we find them... geez.")

elif answer == 2:
    sm.sendNext("Yeah, we'll find them if we search hard! I just know it.")
    response = sm.sendAskAccept("I bet those #r#o3501004##k things got some of those notes... "
                                "I saw you fighting a moment ago, and you were relatively good at it. "
                                "Maybe you can get one or two of those #bSchoolboys' Note#k notes back?")
    if response == 1:
        sm.sendNext("Not all of these notes are going to be useful, but you'll have to read every single one to find to find the clues!\r\n\r\n"
                    "(Defeat #r#o3501004##k monsters, gather #bSchoolboys' Note#k notes, and read them to find clues.)")
        sm.startQuest(parentID)

elif answer == 3:
    sm.sendNext("What use are you then? "
                "You're supposed to be coming up with ideas here!")