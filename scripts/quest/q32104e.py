# [Ellinel Fairy Academy] Fanzy's Magic 2 (32104)

FANZY = 1040002 # NPC ID
MIDSUMMER_NIGHTS_FOREST_ELLINEL_LAKE_SHORE_VER3 = 101070010 # MAP ID

sm.setSpeakerID(FANZY)
sm.sendNext("Did you bring that #b Starlight Crystal#k?")

sm.sendSay("Not bad, not bad. I'll start the incantation.")

sm.completeQuest(parentID)
sm.warp(MIDSUMMER_NIGHTS_FOREST_ELLINEL_LAKE_SHORE_VER3)