# [Ellinel Fairy Academy] Cootie's Suggestion (32109)

HEADMISTRESS_IVANA = 1500001 # NPC ID

sm.setSpeakerID(HEADMISTRESS_IVANA)
answer = sm.sendNext("You think YOU can find the missing children? "
            "How do you propose to do that?\r\n"
            "#L1##bLet's look around the lake. #k#l\r\n"
            "#L2##bWhy don't we use magic? #k#l\r\n"
            "#L3##bI'd like to look through the children's rooms. #k#l")

if answer == 1:
    sm.sendNext("The lake has been searched ten times over at this point. "
                "There's nothing left there to find.")

elif answer == 2:
    sm.sendNext("The forests around Ellinel are full of old magic. "
                "Even if we could detect the kids, we couldn't be sure that the place we discovered would even be in the same spot when we got there.")

elif answer == 3:
    sm.sendNext("Do you really think you can find some clues in their things? "
                "That might actually work...")
    sm.completeQuest(parentID)