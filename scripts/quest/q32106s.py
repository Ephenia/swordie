# [Ellinel Fairy Academy] Ivana's Misunderstanding (32106)

HEADMISTRESS_IVANA = 1500001 # NPC ID
FACULTY_HEAD_KALAYAN = 1500002 # NPC ID 2

sm.setSpeakerID(HEADMISTRESS_IVANA)
sm.sendNext("You're still here. "
            "Is there more to discuss?")

sm.setSpeakerID(FACULTY_HEAD_KALAYAN)
sm.sendSay("You cannot trust this outsider, Headmistress! "
           "The human will only feed us lies and lame excuses.")

sm.setPlayerAsSpeaker()
sm.sendSay("#bI thought you were a wise and rational people.#k "
           "#bWe should analyze the facts before we come to any kind of judgement.#k")

sm.setSpeakerID(FACULTY_HEAD_KALAYAN)
sm.sendSay("Five children vanished into thin air at once! "
           "What other facts do you need? "
           "This one kidnapped them, end of story!")

sm.setPlayerAsSpeaker()
sm.sendSay("#bSo, you have proof Cootie was the culprit?#k")

sm.setSpeakerID(FACULTY_HEAD_KALAYAN)
sm.sendSay("The one you call Cootie has been chased off of these grounds a number of times, but he continues to return and defy our wishes. "
           "He has been conducting secret experiments in our forest!")

sm.sendSay("He's been planning this! "
           "It's a perfect crime. "
           "He comes to scout the area for weeks before he finally steals the children from underneath our very noses! "
           "He knew we had a number of staffers going out on vacation, and I caught him loitering around the scene of the crime afterward. "
           "He MUST be guilty!")

sm.setPlayerAsSpeaker()
sm.sendSay("#b(Could Cootie really have planned the kidnapping of five children?#k "
           "#bHe's so small!)#k")

sm.setSpeakerID(HEADMISTRESS_IVANA)
sm.sendSay("Your desire is to find the most rational explanation. "
           "I present to you that our prime suspect IS the most rational explanation. "
           "We must interrogate him.")

sm.setPlayerAsSpeaker()
sm.sendSay("#b(They're way too upset to see anybody except Cootie as a suspect.#k "
           "#bBetter talk to him...)#k")

sm.startQuest(parentID)