# [Ellinel Fairy Academy] Combing the Academy 2 (32111)

COOTIE_THE_REALLY_SMALL = 1500011 # NPC ID

sm.setSpeakerID(COOTIE_THE_REALLY_SMALL)
response = sm.sendAskAccept("If this note is accurate, the secret thing they were working on will be around the dormitories. "
                            "The boys' hall stretches across most of the second floor. "
                            "Let's try there.")

if response == 1:
    sm.sendNext("I'm, uh, I'm going to hang out here until you're done. "
                "I need to look around...\r\n"
                "#b(Check the dormitories on both ends of the 2nd floor.)#k")
    sm.startQuest(parentID)