# [Ellinel Fairy Academy] Clue Number One (32112)

COOTIE_THE_REALLY_SMALL = 1500011 # NPC ID

sm.setSpeakerID(COOTIE_THE_REALLY_SMALL)
sm.sendNext("You found a script? "
            "Let me take a look at that.\r\n\r\n"
            "...Well there are some obvious problems in the first act, and the All-You-Can-Eat Sundae Bar scene seems a little tacked on, but this is a fine example of fairy entertainment. "
            "Why did the kids have this?")

sm.sendSay("Let's investigate the 3rd floor! "
           "Maybe we'll find something else.\r\n\r\n"
           "#b(Talk to #p1500012##k on the 3rd floor of Ellinel Fairy Academy.)")

sm.completeQuest(parentID)