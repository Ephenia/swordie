# [Ellinel Fairy Academy] Combing the Academy 1 (32110)

COOTIE_THE_REALLY_SMALL = 1500011 # NPC ID

sm.setSpeakerID(COOTIE_THE_REALLY_SMALL)
sm.sendNext("Did you find anything useful?")

sm.sendSay("A secret project? "
           "We'd better look for that from here out.")

sm.completeQuest(parentID)