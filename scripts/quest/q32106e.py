# [Ellinel Fairy Academy] Ivana's Misunderstanding (32106)

COOTIE_THE_REALLY_SMALL = 1500000 # NPC ID

sm.setSpeakerID(COOTIE_THE_REALLY_SMALL)
sm.sendNext("Are you here to chastise me?")

sm.sendSay("Look, hear me out, please? "
           "Why would I ever kidnap a fairy? "
           "SURE, they're THE most evolved and amazing species on the planet, but...")

sm.completeQuest(parentID)