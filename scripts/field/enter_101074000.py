# Field script for entering Ellinel Fairy Academy

FANZYS_MAGIC_2 = 32104 # QUEST ID
MIDSUMMER_NIGHTS_FOREST_ELLINEL_LAKE_SHORE = 101070000 # MAP ID
MIDSUMMER_NIGHTS_FOREST_ELLINEL_LAKE_SHORE_VER3 = 101070010 # MAP ID

sm.lockInGameUI(True)
sm.sendDelay(2000)
sm.chatScript("The forest of fairies seems to materialize from nowhere as you exit the passage.")
sm.sendDelay(3000)
sm.showFieldEffect("Map/Effect.img/temaD/enter/fairyAcademy")
sm.sendDelay(3000)

if sm.hasQuestCompleted(FANZYS_MAGIC_2):
    warp_to = MIDSUMMER_NIGHTS_FOREST_ELLINEL_LAKE_SHORE_VER3
else:
    warp_to = MIDSUMMER_NIGHTS_FOREST_ELLINEL_LAKE_SHORE

sm.warp(warp_to)
sm.lockInGameUI(False)