# Field script for Midsummer Night's Forest: Ellinel Lake Shore (map version 2)

FANZY = 1500010 # NPC ID
YOU_CAN_DO_IT = 32102 # QUEST ID
MIDSUMMER_NIGHTS_FOREST_ELLINEL_LAKE_SHORE = 101070000 # MAP ID

if sm.hasQuest(YOU_CAN_DO_IT):
    sm.lockInGameUI(True)
    sm.removeEscapeButton()
    sm.setPlayerAsSpeaker()
    sm.sendNext("#bBleh!#k "
                "#bI almost drowned!#k")

    sm.setSpeakerID(FANZY)
    sm.sendSay("There must be some kind of enchantment to keep people from swimming across.")

    sm.setPlayerAsSpeaker()
    sm.sendSay("#bYou could of told me that in advance!#k")

    sm.setSpeakerID(FANZY)
    sm.sendSay("I'm not omniscient, and you make a good test subject. "
               "We'll have to find another way.")

    sm.completeQuest(YOU_CAN_DO_IT)
    sm.warp(MIDSUMMER_NIGHTS_FOREST_ELLINEL_LAKE_SHORE)
    sm.lockInGameUI(False)