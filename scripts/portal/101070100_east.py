# East Portal of Midsummer Night's Forest: Radiant Lake Path 1

FANZYS_MAGIC_2 = 32104 # QUEST ID
MIDSUMMER_NIGHTS_FOREST_ELLINEL_LAKE_SHORE = 101070000 # MAP ID
MIDSUMMER_NIGHTS_FOREST_ELLINEL_LAKE_SHORE_VER3 = 101070010 # MAP ID

if sm.hasQuestCompleted(FANZYS_MAGIC_2):
    warp_to = MIDSUMMER_NIGHTS_FOREST_ELLINEL_LAKE_SHORE_VER3
    portal_number = 2
else:
    warp_to = MIDSUMMER_NIGHTS_FOREST_ELLINEL_LAKE_SHORE
    portal_number = 6
sm.warp(warp_to, portal_number)