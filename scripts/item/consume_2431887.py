# Frozen RED (2431887)

items = [
        [4310088, 1], # RED Coin
        [2432005, 1], # Happy Coin Box
        [1012420, 1], # Real Fruit Strawberry Popsicle
        [2049100, 1], # Chaos Scroll
        [2430143, 1], # Unidentified Love Letter
        [2001530, 30], # Reindeer Milk
        [2001531, 30] # Sunrise Dew
        ]

FROZEN_RED = 2431887 # ITEM ID

for i in range (sm.getQuantityOfItem(FROZEN_RED)):
    randomInt = sm.getRandomIntBelow(len(items))
    if sm.canHold(items[randomInt][0]):
        if items[randomInt][0] == 1012420:
            if sm.hasItem(1012420):
                sm.chatBlue("You already had a Real Fruit Strawberry Popsicle, so it was disposed of.")
            else:
                sm.giveItem(items[randomInt][0], items[randomInt][1])
        else:
            sm.giveItem(items[randomInt][0], items[randomInt][1])
        sm.consumeItem(FROZEN_RED)
    else:
        sm.chatBlue("Please check that you have sufficient space in your EQUIP, and USE inventory.")
        break