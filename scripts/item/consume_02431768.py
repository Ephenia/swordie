# Schoolboys' Note (2431768)

responses = ["I think Tracy has a crush on me. "
             "I'm way too popular...",
             "I'm sooo booooored. "
             "When is class over?",
             "Whoever gets this note is an idiot forever!",
             "Race me to the tops of the trees, Phiny! "
             "I'm the best flier in class, no doubt about it!",
             "You're a big dummy!",
             "Shame on you, Tosh! "
             "Holding hands with girls!",
             "So... mesmerizing. "
             "That head... it sparkles...",
             "#bI've hidden my secrets under the bookshelves.#k "
             "#bDon't get caught snooping around!#k"]

SCHOOLBOYS_NOTE = 1500029 # NPC ID
COMBING_THE_ACADEMY_1 = 32110 # QUEST ID
dispose_item = sm.getQuantityOfItem(parentID)

sm.consumeItem(parentID)
sm.removeEscapeButton()

if sm.hasQuest(COMBING_THE_ACADEMY_1):

    randomInt = sm.getRandomIntBelow(8)

    for i in range(len(responses)):
        if randomInt == i:
            sm.setSpeakerID(SCHOOLBOYS_NOTE)
            sm.sendNext(responses[i])
            sm.setPlayerAsSpeaker()
            if randomInt <= 6:
                sm.sendNext("...This is all useless kid stuff! "
                            "How many more notes do I have to read?")
            else:
                sm.sendNext("Secrets under the bookshelves? "
                            "I'd better go ask #bCootie#k about these.")
                sm.createQuestWithQRValue(32133, "1")
                sm.consumeItem(parentID, dispose_item)
            break

else:
    sm.setPlayerAsSpeaker()
    sm.sendNext("Under the bookshelves... "
                "Nooks and crannies... "
                "I've checked everywhere! "
                "No need for these notes now.")
    sm.consumeItem(parentID, dispose_item)